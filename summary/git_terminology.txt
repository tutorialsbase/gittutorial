int
------
Initialize a repository.
Command: git init

Commit
-------
A commit creates a snapshot of current state of files changes(or "delta").
Command: git commit

branching
----------
A branch is an assigned name/pointer for a specific commit.
Command: git branch <branch name>

Relative ref
------------
Its a way to move and checkout into "HEAD", around commits based on their relative positions. There's two way of doing this.
- ^: move up single position from current position, it could be multiple, ie. ^^: two position above
- ~n:move up to nth position from current position
Command: git checkout <relative branch name>^ 
OR: git checkout <relative branch name>~1

Moving a branch forcefully
---------------------------
Command: git branch -f <source branch> <destination branch>
i.e.: git branch -f master HEAD~3  (It moves master branch in 3 position back from HEAD)

Merge
------
Combine two different feature/code-base from two different branches(commit) tree. Merging in Git creates a special commit that has two unique parents.

Command:git merge <branch name to merge into current branch>
(This means, ie. if you are in "master" branch and merge with "my_branch" then it will merge "my_branch" into "master")

Reset
------
Change into previous commit without creating a new commit. Its a local change only.
Command: git reset <destination branch name>

Revert
------
Change into previous commit while creating a new commit. Essentially, it copies the specified previous commit. So its a global change.
command: git revert <desired commit to copy>

Rebase
-------
Rebasing essentially takes a set of commits, "copy(move)" them, and plops them down in hiararchy of another branch.
Command: git rebase <destination branch>

Interactive rebase
------------------
It prompts an interactive UI window to select specific commits to select from. Commits can also be rearranged.
Command: git rebase -i <up to which level the commits should be selected into UI> --aboveAll
i.e.: git rebase -i HEAD~4 --aboveAll 


Cherry-pick
------------
Selecting and copying previous commits above current commit
command: git cherry-pick <Commit1> <Commit2> <...>

Checkout
--------
local: Means switch to a branch. And forward the branch pointer on each commit.
remote: Same as local but don't forward the branch unless remote is updated.
Command local: git checkout <branch_name>
Command remote: git checkout <remote_name>/<remote_branch_name>

Tags
-----
A name like a branch name but with its fixed position. Usually used for version naming.
Command: git tag <tag name> <commit name|branch name>
i.e.: git tag v1 C1

Describe
--------
This command is used to get info about nearest tag and how many commit it lags behind.
Command: git describe <branch/commit name>

Fetch
------
Fetch gets every new commits/changes from remote but don't change the local sate/branch pointer immediately to the updated one.
Command: git fetch 

Pull
-----
Contrary to fetch, pull update local snapshot state that of remote one.
Command: git pull

Push
-----
Git push send the updated commits to the origin/remote repo.
Command: git push 

Blame
-----
See every single change in a file for each commit.